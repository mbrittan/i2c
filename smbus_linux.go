// Copyright 2019 Matt Brittan.  All rights reserved.
// See the LICENSE file for information on permitted usage

// This file encapuslates SMBus busses (does not support i2c only variants)

package i2c

import (
	"fmt"
	"os"
	"sync"
	"unsafe"
)

/*
*
* Structures used when calling the I2C_SMBUS (ioctlSmbus) ioctl command
* The I2C_SMBUS (ioctlSmbus) is used to communicate with SMBus devices
*
 */

// smbus flags (from i2c.h - i2c_smbus_xfer read or write markers)
type smbusRW uint8

// Values for the above
const (
	smbusRead  smbusRW = 0x01 // (I2C_SMBUS_READ)
	smbusWrite smbusRW = 0x00 // (I2C_SMBUS_WRITE)
)

// smbus transactions - identifies the size of the data blocks transferre (from i2c.h - i2c_smbus_xfer read or write markers)
type smbusTransaction uint32

// SMBus Transaction types (from i2c.h) - these identify the size of the data blocks transferred
const (
	smbusQuick          smbusTransaction = 0x00 // (I2C_SMBUS_QUICK)
	smbusByte           smbusTransaction = 0x01 // (I2C_SMBUS_BYTE) - Believe this indicates the data will be passed directly (not a pointer)
	smbusByteData       smbusTransaction = 0x02 //(I2C_SMBUS_BYTE_DATA) - Passing a pointer to a byte
	smbusWordData       smbusTransaction = 0x03 // (I2C_SMBUS_WORD_DATA)
	smbusProcCall       smbusTransaction = 0x04 // (I2C_SMBUS_PROC_CALL)
	smbusBlockData      smbusTransaction = 0x05 // (I2C_SMBUS_BLOCK_DATA)
	smbusI2cBlockBroken smbusTransaction = 0x06 // (I2C_SMBUS_I2C_BLOCK_BROKEN)
	smbusBlockProcCall  smbusTransaction = 0x07 // (I2C_SMBUS_BLOCK_PROC_CALL - SMBus 2.0)
	smbusI2cBlockData   smbusTransaction = 0x08 // (I2C_SMBUS_I2C_BLOCK_DATA)
)

/* C struct that i2cSmbusIoctlData replicates (from i2c-dev.h)
// This is the structure as used in the I2C_SMBUS ioctl call
struct i2c_smbus_ioctl_data {
	__u8 read_write;
	__u8 command;
	__u32 size;
	union i2c_smbus_data __user *data;
};
*/

// smBusCommand is a byte; generally will be the address to read/write
type smBusCommand uint8

// i2cSmbusIoctlData is the structure passed to Ioctl for a ioctlSmbus call
type i2cSmbusIoctlData struct {
	readWrite smbusRW          // Read/Write flag - See above constants
	command   smBusCommand     // The command/register to send to the device
	size      smbusTransaction // transaction type - See above constants (drives contents of below)
	// We always use a pointer for data - this means that this structure cannot be used to directly pass a value (e.g. I2C_SMBUS_BYTE)
	data uintptr // pointer to data structure (type depends upon call can be byte, word or block)
}

const (
	smbusBlockMax = 32
)

/* C union that defines data format for i2cSmbusIoctlData.data (from i2c.h)
#define I2C_SMBUS_BLOCK_MAX	32	// As specified in SMBus standard
union i2c_smbus_data {
	__u8 byte;
	__u16 word;
	__u8 block[I2C_SMBUS_BLOCK_MAX + 2]; // block[0] is used for length
			       // and one more for user-space compatibility
};
*/

/*
*
* Now the relatively simple code that uses the above to send SMBus commands...
*
 */

// smbusConnection encapuslates connection to an SMBus
type smbusConnection struct {
	f     *os.File // File handle to use
	busID string   // The bus name (for logging/debug)

	fn ioctlFunctionality // Passed into us on construction (may be needed in the future to work out supported functions)

	mu sync.Mutex // In theory the kernel probably has an internal lock but not taking any chance.
}

// Close - closes the smbus connection
func (s *smbusConnection) Close() error {
	s.mu.Lock()
	defer s.mu.Unlock()
	if err := s.f.Close(); err != nil {
		return fmt.Errorf("smbusConnection:Close: %s", err)
	}
	return nil
}

// FunctionalityString - Gets a string that details the functionality provided by the I2C bus (for debugging)
func (s *smbusConnection) FunctionalityString() string {
	return s.fn.String()
}

// GetByte - Issue the passed command to the specified device and return the result (one bytes)
func (s *smbusConnection) GetByte(slaveID uint16, address uint8) (byte, error) {
	s.mu.Lock() // We don't want two writes to happen at the same time...
	defer s.mu.Unlock()

	// We set the slave before every write because this may have been changed (suspect that it is held
	// against the file handle but not sure)
	if err := s.setSlave(slaveID); err != nil {
		return 0, fmt.Errorf("smbusConnection:GetByte:%s", err)
	}

	// Request the data...
	var v uint8
	cmd := i2cSmbusIoctlData{
		readWrite: smbusRead,
		command:   smBusCommand(address),
		size:      smbusByteData,
		data:      uintptr(unsafe.Pointer(&v)),
	}
	ptr := unsafe.Pointer(&cmd)

	err := ioctl(s.f, ioctlSmbus, uintptr(ptr))
	if err != nil {
		return 0, fmt.Errorf("smbusConnection:GetByte:%s", err)
	}
	return v, nil
}

// GetBytes - Issue the passed command to the specified device and return the result (one bytes)
func (s *smbusConnection) GetBytes(slaveID uint16, startAddress uint8, buf []byte) error {
	if !s.fn.SupportsSMBusReadBlockData() {
		return s.readBytesIteratively(slaveID, startAddress, buf)
	}

	// The intention here was to use readBlock (one call to get the data in) but this does not
	// seem to work with the devices I have tested on. This may be device specific or could
	// just be my code (but icDump with mode = s also fails)
	// So for now we use readBytesIteratively for everything...
	return s.readBytesIteratively(slaveID, startAddress, buf)
}

// readBlock Retrieves multiple bytes by performing a block read
func (s *smbusConnection) readBlock(slaveID uint16, startAddress uint8, buf []byte) error {
	reqLen := len(buf)
	if reqLen > smbusBlockMax {
		return fmt.Errorf("smbusConnection:GetBytes: SMBus supports max read of %d bytes", smbusBlockMax)
	}

	if !s.fn.SupportsSMBusReadBlockData() {
		return fmt.Errorf("smbusConnection:GetBytes: SMBus does not support block reads")
	}

	s.mu.Lock() // We don't want two writes to happen at the same time...
	defer s.mu.Unlock()

	// We set the slave before every write because this may have been changed (suspect that it is held
	// against the file handle but not sure)
	if err := s.setSlave(slaveID); err != nil {
		return fmt.Errorf("smbusConnection:GetBytes:%s", err)
	}

	// Make a buffer for the response, length will be based on what we expect back and cap will be based on
	// i2cSMBusBlockMax just in case...
	// len is +1 to allow for request length in byte 0
	// cap is +2 because  block[0] is used for length and one more for user-space compatibility (whatever that means!)
	data := make([]byte, reqLen+1, smbusBlockMax+2)
	data[0] = byte(reqLen) // Store number of regs requested

	// Request the data...
	cmd := i2cSmbusIoctlData{
		readWrite: smbusRead,
		command:   smBusCommand(startAddress),
		size:      smbusBlockData,
		data:      uintptr(unsafe.Pointer(&data[0])),
	}
	ptr := unsafe.Pointer(&cmd)
	err := ioctl(s.f, ioctlSmbus, uintptr(ptr))
	if err != nil {
		return fmt.Errorf("smbusConnection:GetBytes: %s", err)
	}

	// Copy buffer (we assume data is all there if no error)
	copy(buf[:reqLen], data[1:reqLen+1])

	return nil
}

// readBytesIteratively uses multiple calls to read smbusByteData to retrieve multiple bytes
func (s *smbusConnection) readBytesIteratively(slaveID uint16, startAddress uint8, buf []byte) error {
	// For performance reasons we make the ioctl calls from here (meaning one mu lock and slave id set)

	// Lets also check that the maximum address is valid..
	if uint16(startAddress)+uint16(len(buf)) > 0xFF {
		return fmt.Errorf("smbusConnection:readBytesIteratively: endAddress (%d) out of range", uint16(startAddress)+uint16(len(buf)))
	}
	reqLen := uint8(len(buf))

	s.mu.Lock() // We don't want two writes to happen at the same time...
	defer s.mu.Unlock()

	// We set the slave before every write because this may have been changed (suspect that it is held
	// against the file handle but not sure)
	if err := s.setSlave(slaveID); err != nil {
		return fmt.Errorf("smbusConnection:readBytesIteratively:%s", err)
	}

	// Lets setup a structure for the ioctl command and reuse it...
	var v uint8
	cmd := i2cSmbusIoctlData{
		readWrite: smbusRead,
		size:      smbusByteData,
		data:      uintptr(unsafe.Pointer(&v)),
	}
	ptr := unsafe.Pointer(&cmd)

	// Retrueve each byte individually
	for i := uint8(0); i < reqLen; i++ {
		cmd.command = smBusCommand(startAddress + i)
		err := ioctl(s.f, ioctlSmbus, uintptr(ptr))
		if err != nil {
			return fmt.Errorf("smbusConnection:readBytesIteratively:%s", err)
		}
		buf[i] = v
	}

	return nil

}

// SetByte - Writes a byte to the specified address on specified device
func (s *smbusConnection) SetByte(slaveID uint16, address uint8, value byte) error {
	s.mu.Lock() // We don't want two writes to happen at the same time...
	defer s.mu.Unlock()

	// We set the slave before every write because this may have been changed (suspect that it is held
	// against the file handle but not sure)
	if err := s.setSlave(slaveID); err != nil {
		return fmt.Errorf("smbusConnection:SetByte:%s", err)
	}

	// Request the data...
	cmd := i2cSmbusIoctlData{
		readWrite: smbusWrite,
		command:   smBusCommand(address),
		size:      smbusByteData,
		data:      uintptr(unsafe.Pointer(&value)),
	}
	ptr := unsafe.Pointer(&cmd)

	err := ioctl(s.f, ioctlSmbus, uintptr(ptr))
	if err != nil {
		return fmt.Errorf("smbusConnection:SetByte:%s", err)
	}
	return nil
}

// setSlave - Selects which slave we are communicating with (should be called before any other action)
// Note: You musty have a lock on my mu before calling! (otherwise another process might jump in!)
// Should also be
func (s *smbusConnection) setSlave(slave uint16) error {
	// smbus supports an 8 bit slaveid...
	if slave > 0xff {
		return fmt.Errorf("setSlave: Invalid smbus slaveid %d", slave)
	}
	err := ioctl(s.f, ioctlSlave, uintptr(uint8(slave)))
	if err != nil {
		fmt.Errorf("setSlave:%s", err)
	}
	return nil
}
