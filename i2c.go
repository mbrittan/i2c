// Copyright 2019 Matt Brittan.  All rights reserved.
// See the LICENSE file for information on permitted usage

// This is a pure go-lang library providing access to i2c devices.
// In addition it supports access to SMBus devices (over i2c).
// Accessing SMBus devices requires the use of slightly different
// system calls from I2C.
//
// Currently this library currently supports LINUX only and has only
// been tested on the amd64 CPUs.
// It is pure go (no CGO) but relies upon ioctl calls that are linux
// specific. Because it is used in projects that support other OS's
// it will build successfully (but every function call will return
// an error).
//
// This library is based (and borrows from) a number of other libraries
// periph.io - library supporting i2c (and heaps more) but not smbus
// github.com/go-daq/smbus - smbus library (no standard i2c support)
//
package i2c

import "errors"

// I2cConnection provides a means to access devices on an I2C bus (including SMBus instances)
// It can be used to communicate with multiple devices from multiple goroutines.
// The interface has been kept limited and simple (I2C and SMBus capabilities differ somewhat)
type I2cConnection interface {
	Close() error                // Close the connection (call this when done!)
	FunctionalityString() string // Returns a string detailing the bus functionality (for debugging)

	// Note: We use Get/Set instead of read/write to avoid using the signitures from io.ByteReader/io.ByteWriter as may want to implement thes in the future
	GetByte(slaveID uint16, address uint8) (byte, error)           // Read a single byte from the device (assumes address = command)
	GetBytes(slaveID uint16, startAddress uint8, buf []byte) error // Read a number of bytes from the device
	SetByte(slaveID uint16, address uint8, value byte) error       // Write a single byte to the device (assumes address = command)
}

// NewI2CConnection opens an I²C bus via its /dev interface as described at
// https://www.kernel.org/doc/Documentation/i2c/dev-interface.
//
// There are alternative ways of accessing I2C/SMBus devices but I believe
// these would either require CGO (complicating builds) or root level access.
//
// You must load the i2c-dev kernal module for this to work! (this provides /dev/i2c* - part of sysfs)
// This can be done temporatly through `modprobe i2c-dev` or permanently using `i2c-dev >> /etc/modules`
//
// The returned I2C struct is threadsafe

// NewI2CConnectionByBusNumber - connect using a bus number, e.g. on linux the X in /dev/i2c-X (linux busnumber 0 = /dev/i2c-0)
func NewI2CConnectionByBusNumber(busNumber int) (I2cConnection, error) {
	if isLinux {
		return newI2CByBusNumber(busNumber)
	}
	return nil, errors.New("sysfs-i2c: is not supported on this platform")
}

// NewI2CConnectionByLocation - connect using a bus location (on linux "0" or "/dev/i2c-0"). String may be platform dependent
func NewI2CConnectionByLocation(busLocation string) (I2cConnection, error) {
	if isLinux {
		return newI2CByLocation(busLocation)
	}
	return nil, errors.New("sysfs-i2c: is not supported on this platform")
}
