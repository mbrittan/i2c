// Copyright 2019 Matt Brittan.  All rights reserved.
// See the LICENSE file for information on permitted usage

// This file encapuslates i2c busses (does not support SMBus variants unless they also support standard i2c access)

package i2c

import (
	"fmt"
	"os"
	"sync"
	"unsafe"
)

/*
*
* Structures used when calling the I2C_RDWR (ioctlRdwr) ioctl command
* The I2C_RDWR (ioctlRdwr) is used to communicate via I2C but is not supported by SMBus busses.
*
 */

/* /* rdwrIoctlData replicates the C struct i2c_rdwr_ioctl_data (from i2c-dev.h) comments from that file below
// This is the structure as used in the I2C_RDWR ioctl call
struct i2c_rdwr_ioctl_data {
	struct i2c_msg __user *msgs;	// pointers to i2c_msgs
	__u32 nmsgs;			// number of i2c_msgs
}
*/

// rdwrIoctlData is the structure passed to Ioctl for a ioctlRdwr call
type rdwrIoctlData struct {
	msgs  uintptr // Pointer to i2cMsg (either one or an array)
	nmsgs uint32  // The number of i2cMsg elements pointed to by the above (array)
}

// flags used in i2cMsg struct (from /i2c-dev.h)
const (
	rdwrflagRD         = 0x0001 // (I2C_M_RD - read data, from slave to master; I2C_M_RD is guaranteed to be 0x0001)
	rdwrflagTEN        = 0x0010 // (I2C_M_TEN - this is a ten bit chip address; WARNING comment in i2c-dev.h that 10-bit addresses are not supported)
	rdwrflagDmaSafe    = 0x0200 // (I2C_M_DMA_SAFE - the buffer of this message is DMA safe makes only sense in kernelspace userspace buffers are copied anyway)
	rdwrflagRecvLen    = 0x0400 // (I2C_M_RECV_LEN - length will be first received byte)
	rdwrflagNoRDACK    = 0x0800 // (I2C_M_NO_RD_ACK - if I2C_FUNC_PROTOCOL_MANGLING)
	rdwrflagIgnoreNAK  = 0x1000 // (I2C_M_IGNORE_NAK - if I2C_FUNC_PROTOCOL_MANGLING)
	rdwrflagRevDirAddr = 0x2000 // (I2C_M_REV_DIR_ADDR - if I2C_FUNC_PROTOCOL_MANGLING)
	rdwrflagNOSTART    = 0x4000 // (I2C_M_NOSTART - if I2C_FUNC_NOSTART)
	rdwrflagSTOP       = 0x8000 // (I2C_M_STOP - if I2C_FUNC_PROTOCOL_MANGLING)
)

/* i2cMsg replicates the C struct i2c_msg (from i2c.h) comments from that file below
 * struct i2c_msg - an I2C transaction segment beginning with START
 * @addr: Slave address, either seven or ten bits.  When this is a ten
 *	bit address, I2C_M_TEN must be set in @flags and the adapter
 *	must support I2C_FUNC_10BIT_ADDR.
 * @flags: I2C_M_RD is handled by all adapters.  No other flags may be
 *	provided unless the adapter exported the relevant I2C_FUNC_*
 *	flags through i2c_check_functionality().
 * @len: Number of data bytes in @buf being read from or written to the
 *	I2C slave address.  For read transactions where I2C_M_RECV_LEN
 *	is set, the caller guarantees that this buffer can hold up to
 *	32 bytes in addition to the initial length byte sent by the
 *	slave (plus, if used, the SMBus PEC); and this value will be
 *	incremented by the number of block data bytes received.
 * @buf: The buffer into which data is read, or from which it's written.
 *
 * An i2c_msg is the low level representation of one segment of an I2C
 * transaction.  It is visible to drivers in the @i2c_transfer() procedure,
 * to userspace from i2c-dev, and to I2C adapter drivers through the
 * @i2c_adapter.@master_xfer() method.
 *
 * Except when I2C "protocol mangling" is used, all I2C adapters implement
 * the standard rules for I2C transactions.  Each transaction begins with a
 * START.  That is followed by the slave address, and a bit encoding read
 * versus write.  Then follow all the data bytes, possibly including a byte
 * with SMBus PEC.  The transfer terminates with a NAK, or when all those
 * bytes have been transferred and ACKed.  If this is the last message in a
 * group, it is followed by a STOP.  Otherwise it is followed by the next
 * @i2c_msg transaction segment, beginning with a (repeated) START.
 *
 * Alternatively, when the adapter supports I2C_FUNC_PROTOCOL_MANGLING then
 * passing certain @flags may have changed those standard protocol behaviors.
 * Those flags are only for use with broken/nonconforming slaves, and with
 * adapters which are known to support the specific mangling options they
 * need (one or more of IGNORE_NAK, NO_RD_ACK, NOSTART, and REV_DIR_ADDR).

struct i2c_msg {
	__u16 addr;	// slave address
	__u16 flags;	// MB - see comments on flags above
	__u16 len;		// msg length
	__u8 *buf;		// pointer to msg data
};
*/

// i2cMsg is the building block for the msgs pointer in rdwrIoctlData
type i2cMsg struct {
	addr   uint16 // Address to communicate with
	flags  uint16 // 1 for read, see i2c.h for more details
	length uint16
	buf    uintptr
}

/*
*
* Now the relatively simple code that uses the above to send I2C commands...
*
 */

// i2cConnection encapuslates connection to an I2C bus
type i2cConnection struct {
	f     *os.File // File handle to use
	busID string   // The bus number (for logging/debug)

	fn ioctlFunctionality // Passed into us on construction (may be needed in the future to work out supported functions)

	mu sync.Mutex // In theory the kernel probably has an internal lock but not taking any chance.
}

// Close - closes the smbus connection
func (i *i2cConnection) Close() error {
	i.mu.Lock()
	defer i.mu.Unlock()
	if err := i.f.Close(); err != nil {
		return fmt.Errorf("i2cConnection:Close: %s", err)
	}
	return nil
}

// FunctionalityString - Gets a string that details the functionality provided by the I2C bus (for debugging)
func (i *i2cConnection) FunctionalityString() string {
	return i.fn.String()
}

// GetByte - Issue the passed command to the specified device and return the result (one bytes)
func (i *i2cConnection) GetByte(slaveID uint16, address uint8) (byte, error) {
	buf := make([]byte, 1)
	err := i.tx(slaveID, []byte{address}, buf)
	if err != nil {
		return 0, fmt.Errorf("i2cConnection:GetByte: %s", err)
	}
	return buf[0], nil
}

// GetBytes - Issue the passed command to the specified device pushing result into the passed buffer
func (i *i2cConnection) GetBytes(slaveID uint16, startAddress uint8, buf []byte) error {
	err := i.tx(slaveID, []byte{startAddress}, buf)
	if err != nil {
		return fmt.Errorf("i2cConnection:GetBytes: %s", err)
	}
	return nil
}

// SetByte - Issue the passed command sending the passed paramater (generally sets a register)
func (i *i2cConnection) SetByte(slaveID uint16, address uint8, value byte) error {
	err := i.tx(slaveID, []byte{address, value}, nil)
	if err != nil {
		return fmt.Errorf("i2cConnection:SetByte: %s", err)
	}
	return nil
}

// Tx execute a write and read as a single transacrion
// This is covers mose use cases because in order to read a value we first need to write which registre we want read
// Note: This code is closely based on periph.io/x/periph/host/sysfs/i2c.go
func (i *i2cConnection) tx(slaveAddr uint16, w, r []byte) error {
	if !(slaveAddr < 0x80) {
		// Consider adding support for the 10 bit addresses (but from comments in the kernal code I suspect this does not work so
		// have not implemented... No testing preformed to verify this)
		// see https://www.kernel.org/doc/Documentation/i2c/ten-bit-addresses
		// || (addr < 0x400 && i.fn.Supports10BitAddress()) {
		return fmt.Errorf("tx: invalid slave address (%d)", slaveAddr)
	}
	if len(w) == 0 && len(r) == 0 {
		return nil
	}

	// Convert parmans into structures needed for the ioctl calls
	var buf [2]i2cMsg
	msgs := buf[0:0]
	if len(w) != 0 {
		msgs = buf[:1]
		buf[0].addr = slaveAddr
		buf[0].length = uint16(len(w))
		buf[0].buf = uintptr(unsafe.Pointer(&w[0]))
	}
	if len(r) != 0 {
		l := len(msgs)
		msgs = msgs[:l+1] // extend the slice by one
		buf[l].addr = slaveAddr
		buf[l].flags = rdwrflagRD
		buf[l].length = uint16(len(r))
		buf[l].buf = uintptr(unsafe.Pointer(&r[0]))
	}
	p := rdwrIoctlData{
		msgs:  uintptr(unsafe.Pointer(&msgs[0])),
		nmsgs: uint32(len(msgs)),
	}
	pp := uintptr(unsafe.Pointer(&p))
	i.mu.Lock()
	defer i.mu.Unlock()
	if err := ioctl(i.f, ioctlRdwr, pp); err != nil {
		return fmt.Errorf("tx: %v", err)
	}
	return nil
}
