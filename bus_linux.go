// Copyright 2019 Matt Brittan.  All rights reserved.
// See the LICENSE file for information on permitted usage

// This file handles access to the i2c bus - enabling iteration and connection to be requested
// When a connection is made then this will be handled by i2x_linux.go or smbus_linux.go depending
// upon the bus functionality (the methods used to read/write are quite different so its beneficial
// to seperate this code)
//
// The aim here is to provide a basic library that does what we need and only what we need!

package i2c

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"unsafe"
)

const isLinux = true // Allow other code to easily tell if we are running on Linux...

// newI2CByBusNumber - Connect to the I2C bus using a bus number, determine functionality, and return an appropriate handler
func newI2CByBusNumber(busNumber int) (I2cConnection, error) {
	fname := fmt.Sprintf("/dev/i2c-%d", busNumber)
	return connect(fname)
}

// newI2CByLocation - Connect to the I2C bus using a string (OS dependent), determine functionality, and return an appropriate handler
func newI2CByLocation(busLocation string) (I2cConnection, error) {
	// We might be passed a string containing a number (bus "0") or a path "/dev/i2c-0"
	// The simplest way to work out which seems to be to get info on all busses then check if any matches...
	bussees, err := GetBusInfo()
	if err != nil {
		return nil, fmt.Errorf("newI2CByLocation: Could not get bus info: %s", err)
	}

	for _, bus := range bussees {
		if bus.Name == busLocation || strconv.Itoa(bus.Number) == busLocation {
			return connect(bus.Name)
		}
	}
	return nil, fmt.Errorf("newI2CByLocation: bus %s not found", busLocation)
}

// connect - Connexct to the I2C bus and return appropriate handler
func connect(fname string) (I2cConnection, error) {
	f, err := os.OpenFile(fname, os.O_RDWR, 0600)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, fmt.Errorf("connect: I2C bus %s does not exist. Do you need to run `modprobe i2c-dev`?", fname)
		}
		return nil, fmt.Errorf("connect: Failed to open bus %s: %s", fname, err)
	}

	// We need to know tha capabilities of the bus because this determines
	// how we access devices on it.
	var fn ioctlFunctionality
	if err = ioctl(f, ioctlFuncs, uintptr(unsafe.Pointer(&fn))); err != nil {
		return nil, fmt.Errorf("connect: %s", err)
	}

	// Lets create the appropriate instance... (giving i2c priotity as we can read/write multiple bytes in one gor easily)
	if fn.SupportsI2C() {
		return &i2cConnection{f: f, busID: fname, fn: fn}, nil
	} else if fn.SupportsSMBusReadByte() && fn.SupportsSMBusWriteByte() {
		return &smbusConnection{f: f, busID: fname, fn: fn}, nil
	}

	// We cannot support this bus...
	f.Close()
	return nil, fmt.Errorf("connect: Bus %s does not support required functionality (%s)", fname, fn.String())
}

//
// Code to iterate I2C Busses
//

// i2cBus references an I²C bus.
//
// It is returned by GetBusInfo() to enumerate all registered buses.
type i2cBus struct {
	// Name of the bus - e.g. /dev/ic2-0. Unique across the host.
	Name string

	// Number of the bus or -1 if the bus doesn't have any "native" number
	// (have not encountered this but think USB based bus may not have a number).
	Number int
}

// GetBusInfo retrieves information on the I2C busses available to us
// This code is mostly taken from https://github.com/google/periph/blob/master/conn/i2c/i2creg/i2creg.go
func GetBusInfo() ([]i2cBus, error) {
	// Do not use "/sys/bus/i2c/devices/i2c-" as Raspbian's provided udev rules
	// only modify the ACL of /dev/i2c-* but not the ones in /sys/bus/...
	prefix := "/dev/i2c-"
	items, err := filepath.Glob(prefix + "*")
	if err != nil {
		return nil, err
	}
	if len(items) == 0 {
		return nil, errors.New("no I²C bus found (you may need to run `modprobe i2c-dev`")
	}
	// Lets return the devices in order...
	sort.Strings(items)

	var busInfo []i2cBus
	for _, item := range items {
		busNo, err := strconv.Atoi(item[len(prefix):])
		// We ignore any errors; if they all fail we will just return an empty list
		if err != nil {
			continue
		}
		busInfo = append(busInfo, i2cBus{
			Name:   item,
			Number: busNo,
		})
	}
	return busInfo, nil
}
