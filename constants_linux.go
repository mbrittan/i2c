// Copyright 2019 Matt Brittan.  All rights reserved.
// See the LICENSE file for information on permitted usage

// This file defines a range of constants used for IOCTL commands
// These are defined in https://github.com/torvalds/linux/blob/master/include/uapi/linux/i2c-dev.h (and i2c.h in the same folder)
// some code taken from periph.io (saved retyping!)
package i2c

import (
	"strings"
)

// We will be communicating with the I2C device via dev/i2c-X using IOCTL commands. These commands
// come from i2c-dev.h and the C constand is in brackets (along with comments from the .h file)
// Main comment from i2c-dev.h follows
// /dev/i2c-X ioctl commands from i2c-dev.h.  The ioctl's parameter is always an unsigned long, except for:
// *	- I2C_FUNCS, takes pointer to an unsigned long
// *	- I2C_RDWR, takes pointer to struct i2c_rdwr_ioctl_data
// *	- I2C_SMBUS, takes pointer to struct i2c_smbus_ioctl_data
type i2cIoctlCmd uint

// Values for the above (from i2c-dev.h)
const (
	ioctlRetries i2cIoctlCmd = 0x701 // (I2C_RETRIES - number of times a device address should be polled when not acknowledging)
	ioctlTimeout i2cIoctlCmd = 0x702 // (I2C_TIMEOUT - set timeout in units of 10 ms)
	// Warning: i2c-dev.h says "NOTE: Slave address is 7 or 10 bits, but 10-bit addresses are NOT supported! (due to code brokenness)"
	ioctlSlave   i2cIoctlCmd = 0x703 // (I2C_SLAVE - Use this slave addres)
	ioctlTenBits i2cIoctlCmd = 0x704 // (I2C_TENBIT - 0 for 7 bit addrs, != 0 for 10 bit)
	ioctlFuncs   i2cIoctlCmd = 0x705 // (I2C_FUNCS - Get the adapter functionality mask)
	ioctlRdwr    i2cIoctlCmd = 0x707 // (I2C_RDWR - Combined R/W transfer (one STOP only))
	ioctlSmbus   i2cIoctlCmd = 0x720 // (I2C_SMBUS - SMBus transfer)
)

/*
*
* Functionality related constants/code (enables us to determine the functionality of a bus)
*
 */

// ioctlFunctionality is used to hold the value returned by a ioctlFuncs call. This is needed to determine whether
// we can use I2C commands or need to use SMBus commands. Also for I2C commands we need this to check if 10 bit
// addresses can be used.
type ioctlFunctionality uint64

// The following constants identify the meanings of the bits in the above - comment in brackets is the
//define in i2c.h
const (
	funcI2C                 ioctlFunctionality = 0x00000001 // (I2C_FUNC_I2C)
	func10BitAddr           ioctlFunctionality = 0x00000002 // (I2C_FUNC_10BIT_ADDR)
	funcProtocolMangling    ioctlFunctionality = 0x00000004 // (I2C_FUNC_PROTOCOL_MANGLING - I2C_M_IGNORE_NAK etc.)
	funcSMBusPEC            ioctlFunctionality = 0x00000008 // (I2C_FUNC_SMBUS_PEC)
	funcNOSTART             ioctlFunctionality = 0x00000010 // (I2C_M_NOSTART)
	funcSlave               ioctlFunctionality = 0x00000020 // (I2C_FUNC_SLAVE)
	funcSMBusBlockProcCall  ioctlFunctionality = 0x00008000 // (I2C_FUNC_SMBUS_BLOCK_PROC_CALL - SMBus 2.0)
	funcSMBusQuick          ioctlFunctionality = 0x00010000 // (I2C_FUNC_SMBUS_QUICK)
	funcSMBusReadByte       ioctlFunctionality = 0x00020000 // (I2C_FUNC_SMBUS_READ_BYTE)
	funcSMBusWriteByte      ioctlFunctionality = 0x00040000 // (I2C_FUNC_SMBUS_WRITE_BYTE)
	funcSMBusReadByteData   ioctlFunctionality = 0x00080000 // (I2C_FUNC_SMBUS_READ_BYTE_DATA)
	funcSMBusWriteByteData  ioctlFunctionality = 0x00100000 // (I2C_FUNC_SMBUS_WRITE_BYTE_DATA)
	funcSMBusReadWordData   ioctlFunctionality = 0x00200000 // (I2C_FUNC_SMBUS_READ_WORD_DATA)
	funcSMBusWriteWordData  ioctlFunctionality = 0x00400000 // (I2C_FUNC_SMBUS_WRITE_WORD_DATA)
	funcSMBusProcCall       ioctlFunctionality = 0x00800000 // (I2C_FUNC_SMBUS_PROC_CALL)
	funcSMBusReadBlockData  ioctlFunctionality = 0x01000000 // (I2C_FUNC_SMBUS_READ_BLOCK_DATA)
	funcSMBusWriteBlockData ioctlFunctionality = 0x02000000 // (I2C_FUNC_SMBUS_WRITE_BLOCK_DATA)
	funcSMBusReadI2CBlock   ioctlFunctionality = 0x04000000 // (I2C_FUNC_SMBUS_READ_I2C_BLOCK - I2C-like block xfer)
	funcSMBusWriteI2CBlock  ioctlFunctionality = 0x08000000 // (I2C_FUNC_SMBUS_WRITE_I2C_BLOCK - w/ 1-byte reg. addr.)
	funcSMBushostNotify     ioctlFunctionality = 0x10000000 // (I2C_FUNC_SMBUS_HOST_NOTIFY)

)

// String - Return the bus capabilities as a string (for debugging /info)
func (f ioctlFunctionality) String() string {
	var out []string
	if f&funcI2C != 0 {
		out = append(out, "I2C")
	}
	if f&func10BitAddr != 0 {
		out = append(out, "10BIT_ADDR")
	}
	if f&funcProtocolMangling != 0 {
		out = append(out, "PROTOCOL_MANGLING")
	}
	if f&funcSMBusPEC != 0 {
		out = append(out, "SMBUS_PEC")
	}
	if f&funcNOSTART != 0 {
		out = append(out, "NOSTART")
	}
	if f&funcSlave != 0 {
		out = append(out, "SLAVE")
	}
	if f&funcSMBusBlockProcCall != 0 {
		out = append(out, "SMBUS_BLOCK_PROC_CALL")
	}
	if f&funcSMBusQuick != 0 {
		out = append(out, "SMBUS_QUICK")
	}
	if f&funcSMBusReadByte != 0 {
		out = append(out, "SMBUS_READ_BYTE")
	}
	if f&funcSMBusWriteByte != 0 {
		out = append(out, "SMBUS_WRITE_BYTE")
	}
	if f&funcSMBusReadByteData != 0 {
		out = append(out, "SMBUS_READ_BYTE_DATA")
	}
	if f&funcSMBusWriteByteData != 0 {
		out = append(out, "SMBUS_WRITE_BYTE_DATA")
	}
	if f&funcSMBusReadWordData != 0 {
		out = append(out, "SMBUS_READ_WORD_DATA")
	}
	if f&funcSMBusWriteWordData != 0 {
		out = append(out, "SMBUS_WRITE_WORD_DATA")
	}
	if f&funcSMBusProcCall != 0 {
		out = append(out, "SMBUS_PROC_CALL")
	}
	if f&funcSMBusReadBlockData != 0 {
		out = append(out, "SMBUS_READ_BLOCK_DATA")
	}
	if f&funcSMBusWriteBlockData != 0 {
		out = append(out, "SMBUS_WRITE_BLOCK_DATA")
	}
	if f&funcSMBusReadI2CBlock != 0 {
		out = append(out, "SMBUS_READ_I2C_BLOCK")
	}
	if f&funcSMBusWriteI2CBlock != 0 {
		out = append(out, "SMBUS_WRITE_I2C_BLOCK")
	}
	if f&funcSMBushostNotify != 0 {
		out = append(out, "SMBUS_HOST_NOTIFY")
	}
	return strings.Join(out, "|")
}

// SupportsI2C returns true if the bus supports I2C commands
func (f ioctlFunctionality) SupportsI2C() bool {
	return f&funcI2C != 0
}

// SupportsSMBusReadByte returns true if the bus supports reading bytes via SMBus commands
func (f ioctlFunctionality) SupportsSMBusReadByte() bool {
	return f&funcSMBusReadByteData != 0
}

// SupportsSMBusReadBlockData returns true if the bus supports reading block data via SMBus commands
func (f ioctlFunctionality) SupportsSMBusReadBlockData() bool {
	return f&funcSMBusReadBlockData != 0
}

// SupportsSMBusWriteByte returns true if the bus supports writing bytes via SMBus commands
func (f ioctlFunctionality) SupportsSMBusWriteByte() bool {
	return f&funcSMBusWriteByteData != 0
}

// Supports10BitAddress returns true if the bus supports 10 bit addresses
// Warning: i2c-dev.h says "NOTE: Slave address is 7 or 10 bits, but 10-bit addresses are NOT supported! (due to code brokenness)"
func (f ioctlFunctionality) Supports10BitAddress() bool {
	return f&func10BitAddr != 0
}
