# i2c - Library to connect to I2C bus and allow access to I2C and SMBus devices

This package aims to provide a simple method to access I2C and SMBus devices. The functionality
is limited to what I required at the time it was created but it should be commented well enough 
for others to extend it to meet their needs (have included quite a bit C code in the comments to make 
it easy to find the relevant bits in the kernel code).

Currently supports Linux only and has only been tested with a small number of devices connected to 
amd64 linux machines.

*Note: While only linux is supported the code has been written so that it will compile on any
platform (but will always return an error if used).*

Thanks to the authors of periph.io (have copied/pasted a hunk of code) and github.com/go-daq/smbus 
(helped me work out the smbus calls).