// Copyright 2019 Matt Brittan.  All rights reserved.
// See the LICENSE file for information on permitted usage

// This file contains functions that perform low level file operations

package i2c

import (
	"os"
	"syscall"
)

// ioctl sends an ioctl to the file handle.
func ioctl(f *os.File, op i2cIoctlCmd, data uintptr) error {
	/* Probably need this on a mips system (see periph.io/x/periph/host/fs/fs.go)
	if isMIPS {
		var err error
		if op, err = translateOpMIPS(op); err != nil {
			return err
		}
	}*/

	if _, _, errno := syscall.Syscall(syscall.SYS_IOCTL, f.Fd(), uintptr(op), data); errno != 0 {
		return syscall.Errno(errno)
	}
	return nil
}
