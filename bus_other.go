// Copyright 2019 Matt Brittan.  All rights reserved.
// See the LICENSE file for information on permitted usage

// Linux is the only OS supported at the moment - for non-linux OS we just return an error for now
// This means the code will compile but not run...

// +build !linux

package i2c

import (
	"errors"
)

const isLinux = false // Allow other code to easily tell if we are running on Linux...

// Ref references an I²C bus.
//
// It is returned by All() to enumerate all registered buses.
type i2cBus struct {
}

// GetBusInfo retrieves information on the I2C busses available to us
// This code is mostly taken from https://github.com/google/periph/blob/master/conn/i2c/i2creg/i2creg.go
func GetBusInfo() ([]i2cBus, error) {
	return nil, errors.New("I2C Support is currently only available on Linux OS")
}

// newI2CByBusNumber - always fails as we only support linux currently...
func newI2CByBusNumber(_ int) (I2cConnection, error) {
	return nil, errors.New("I2C Support is currently only available on Linux OS")
}

// newI2CByLocation - always fails as we only support linux currently...
func newI2CByLocation(_ string) (I2cConnection, error) {
	return nil, errors.New("I2C Support is currently only available on Linux OS")
}
